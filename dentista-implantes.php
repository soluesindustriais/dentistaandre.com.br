<?php
include('inc/vetKey.php');
$h1 = "dentista implantes";
$title = $h1;
$desc = "Dentista implantes Por serem integrados ao osso, os implantes oferecem um suporte estável para os dentes artificiais. Próteses parciais e totais";
$key = "dentista,implantes";
$legendaImagem = "Foto ilustrativa de dentista implantes";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>O que faz um dentista implantes</h2><p>O implante é fixado junto a arcada dentária do paciente, o que deixa mais estável e seguros na boca. Graças a essa característica, os implantes não têm a possibilidade de escorregar ou se mexerem dentro da boca, o que leva ainda mais conforto e segurança para a realização do tratamento.</p><p>Esse tipo de implante é chamado de prótese sobre implante, ela leva  mais segurança até o paciente, proporcionando melhores funções da boca, bem próximas as dos dentes naturais. Esse tipo de procedimento só pode ser realizado por um dentista implantes especializado.</p><p>Para uma grande quantidade de pessoas, dentaduras ou até mesmo próteses são extremos casos de desconfortos, podendo ser até mesmo impossíveis de se realizar, muito desse preconceito se da por conta dos pontos e ao processo de adaptação que no começo é complicado, porém logo em seguida fica como se fosse natural. Para implantar essas próteses, não é preciso  nenhuma preparação do dente natural, diferentemente do que é feito em pontes fixas.</p><p>O paciente que irá receber um implante precisa estar com as gengivas totalmente saudáveis, e a arcada dentária o mais adequada possível para sustentar a prótese. É muito importante o paciente saber que, após colocar uma prótese a higiene deve ser redobrada, realizada por mais vezes ao dia com ainda mais cautela, essa é uma das receitas para a duração do um implante de sucesso.</p><h2>As melhores dicas para um bom cuidado bucal, quando se tem um implante</h2><p>É difícil apontar um implante que não tenha sua eficiência.  Podem existir casos de que o sucesso daquela etapa do implante seja total por conta do paciente, e não somente do dentista implantes. O profissional aponta diversas dicas que podem auxiliar e muito com o cuidado bucal quando se tem um implante envolvido:</p><ul><li>Praticar a higiene bucal de forma adequada – Procure sempre escovar os dentes três vezes por dia, ou sempre após a refeição, utilize o fio dental da maneira correta diariamente;</li></ul><ul><li>Evite cigarros– O tabaco pode enfraquecer a estrutura óssea, assim contribuindo para falhas no implante;</li></ul><ul><li>Mantenha uma rotina com seu dentista implantes – Realizar limpezas e exames com o tempo determinado por ele podem ajudar a garantir que seu implante esteja sempre em ótimas condições;</li></ul><ul><li>Cuidado ao comer alimentos duros – Evite ao máximo  mastigar itens duros como gelo ou maça do amor, uma vez que eles oferecem risco de quebrar seus dentes naturais.</li></ul><h2>Qual a duração de um implante?</h2><p align="center"></p><p>Com análises feitas por especialistas da área, um procedimento bem realizado pode durar cerca de até 3 horas. O mesmo é colocado através de pinos de titânio, que tem como principal característica sua durabilidade. Mas é sempre bom ressaltar que o tratamento pode ter variações, indo de caso a caso e de dentista para dentista implantes. </p><p>Quando a boca que receberá o implante está em sua mais perfeita condição, o procedimento tende ser ainda mais rápido. Para poder ter esse prazer novamente, procure um bom dentista implantes, marque uma consulta e opte pelo melhor tratamento para sua saúde bucal.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
