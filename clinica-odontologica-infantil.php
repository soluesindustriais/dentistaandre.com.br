<?php
include('inc/vetKey.php');
$h1 = "clínica odontológica infantil";
$title = $h1;
$desc = "Clínica odontológica infantil A saúde bucal é uma parte muito importante da saúde geral da criança, mesmo assim, às vezes é negligenciada em crianças";
$key = "clínica,odontológica,infantil";
$legendaImagem = "Foto ilustrativa de clínica odontológica infantil";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Clínica odontológica infantil</h2><p align="center"></p><p>A saúde bucal é uma parte muito importante da saúde geral da criança, mesmo assim, às vezes é negligenciada em crianças pequenas. Boas práticas de saúde bucal deveriam começar com uma consulta odontológica inicial antes do primeiro aniversário da criança.</p><p>Os dentes da criança devem durar uma vida inteira e com cuidado apropriado, uma dieta balanceada e consultas odontológicas regulares, as crianças podem ter uma vida inteira de sorrisos saudáveis.</p><p>É recomendado por especialistas check-ups odontológicos regulares em uma clínica odontológica infantil especializada, incluindo uma consulta com o dentista em até seis meses após a irrupção do primeiro dente e não mais tardar do que no primeiro aniversário da criança. Cuidados preventivos, como profilaxias e, se necessário, tratamentos com flúor, constituem o "seguro sorriso" das crianças. Exames dentais de rotina descobrem problemas que podem ser facilmente tratados nos estágios iniciais, quando o dano é mínimo.</p><h2>O que a criança pode esperar ao ir em uma clínica odontológica infantil?</h2><p align="center"></p><p>Em sua primeira visita à clínica odontológica infantil, o dentista vai verificar o histórico de saúde dental completo da criança. Em visitas de acompanhamento, se o seu status de saúde for modificado, não se esqueça de contar ao seu dentista. A seguir, está o que você pode esperar durante a maioria das visitas à clínica odontológica infantil.</p><ul><li>Limpeza minuciosa: O dentista vai raspar ao longo e abaixo da margem gengival para remover a placa bacteriana e o tártaro formado que pode causar doenças da gengiva, mau hálito e outros problemas. Em seguida, ele vai polir e utilizar o fio dental em seus dentes;</li></ul><ul><li>Exame dental completo: Seu dentista vai realizar um exame minucioso em seus dentes, gengiva e boca, observando sinais de doenças ou outros problemas;</li></ul><ul><li>Raios-X: Raios-X podem diagnosticar problemas de outra forma não observáveis, como danos as mandíbulas, dentes impactados, abscesso, cistos ou tumores e cárie entre os dentes;</li></ul><ul><li>Verificação de Cáries: Devido aos grandes hábitos de consumir doces e não ter uma informação correta de como realizar uma boa higiene bucal, a criança pode se encontrar com cárie, sendo assim, é passada uma massa própria para tampar esse buraco formado na parte externa do dente. Evitando assim, problemas futuros.</li></ul><h2>O material mais usado na clínica odontológica infantil</h2><p align="center"></p><p>Selantes dentais são usados para proteger as superfícies mastigatórias contra a cárie dentária, que é a doença crônica mais comum da infância. Seu dentista pode ajudar a prevenir ou reduzir a incidência de cárie dentária aplicando selantes nos dentes do seu filho.</p><p>Um selante é um material plástico transparente ou da cor do dente que é aplicado nas superfícies mastigatórias dos dentes posteriores, onde a cárie dentária ocorre com mais frequência. Os selantes protegem sulcos e depressões normais dos dentes, que são denominadas fóssulas e fissuras dentárias, áreas que são particularmente suscetíveis à cárie dentária.</p><p>Qualquer criança envolvida em atividade recreativa, como futebol, andar de skate, ou mesmo bicicleta, deve usar um protetor bucal. Trata-se de protetores de “estoque” disponíveis em lojas, e uma variedade feita sob medida pelo dentista que permite melhor adaptação. Pergunte ao seu dentista sobre o uso de protetor bucal, e utilize esse material como proteção e cuidados aos seus dentes.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
