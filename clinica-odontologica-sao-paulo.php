<?php
include('inc/vetKey.php');
$h1 = "clínica odontológica são paulo";
$title = $h1;
$desc = "Clínica odontológica são paulo Cuidar de seus dentes envolve muito mais que escová-los e passar o fio dental. Para um cuidado completo, é importante";
$key = "clínica,odontológica,são,paulo";
$legendaImagem = "Foto ilustrativa de clínica odontológica são paulo";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Clínica odontológica são paulo</h2><p>Cuidar de seus dentes envolve muito mais que escová-los e passar o fio dental. Para um cuidado completo, é importante ir a uma clínica odontológica são paulo a cada seis meses para uma consulta de rotina e limpeza profissional.</p><p>O primeiro passo nesse processo é encontrar uma clínica odontológica são paulo com o qual você se sinta confortável, e então agendar uma consulta. A maioria das visitas à clínica odontológica são paulo são check-ups. Consultas regulares (idealmente a cada seis meses) e limpezas profissionais ajudarão a manter seus dentes e gengiva mais limpos e resistentes, além de prevenir problemas que surgem com o envelhecimento.</p><p>Precisa trazer seu filho pequeno para uma consulta? Consultórios odontológicos pediátricos normalmente oferecem um ambiente projetado para deixar as crianças confortáveis. As salas podem ser decoradas com cores brilhantes, animais ou desenhos divertidos. As salas de espera contam normalmente com uma variedade de brinquedos.</p><p>Eles podem ter até mesmo mesas de atividades ou videogame. Um ambiente divertido no consultório odontológico torna a visita algo divertido para as crianças. Eles terão paciência e se entreterão até que chegue sua vez, e podem até mesmo pedir para voltar mais cedo que o necessário para que possam brincar mais.</p><h2>Como será sua primeira visita a uma clínica odontológica são paulo</h2><p align="center"></p><p>Em sua primeira visita, o dentista obterá um histórico médico completo. Nas visitas seguintes, caso seu estado de saúde tenha se alterado, informe ao dentista. Veja a seguir o que você pode esperar na maioria das consultas a clínica odontológica são paulo. Dependendo de seu histórico de saúde bucal, sua experiência pode ser distinta.</p><ul><li>Uma limpeza completa: Uma consulta quase sempre inclui uma limpeza dentária completa com um dentista. Utilizando instrumentos especiais, o dentista raspa ao longo da margem da gengiva e abaixo dela, removendo a placa (biofilme dental) acumulada e o tártaro (cálculos dentais), que pode causar doença gengival, cárie, mau hálito e outros problemas. O dentista irá polir seus dentes e passar o fio dental neles durante a consulta e lhe instruirá a respeito de técnicas de cuidado bucal e produtos a serem utilizados em casa para uma melhor saúde bucal;</li></ul><ul><li>Um exame dentário completo: O dentista realizará um exame completo de seus dentes, gengiva e boca, procurando por sinais de doenças e de outros problemas. O objetivo é ajudar a manter uma boa saúde bucal e evitar que problemas se tornem sérios, identificando-os e tratando-os o mais rápido possível;</li></ul><ul><li>Radiografias: Dependendo de sua idade, riscos de saúde bucal e sintomas, o dentista pode recomendar tirar radiografias. Eles podem diagnosticar problemas que passariam despercebidos, como danos ao osso da mandíbula, dentes inclusos, abscessos, cistos ou tumores, e cárie entre os dentes;</li></ul><ul><li>E possíveis aplicações de flúor: Ao atingir o dente, o flúor é absorvido pelo esmalte. Isso ajuda a reparar o esmalte através da recomposição do cálcio e fósforo perdidos para manter os dentes duros. </li></ul><h2>Com que frequência deve-se ir a uma clínica odontológica são paulo?</h2><p align="center"></p><p>Se seus dentes e gengivas estiverem em boas condições, você poderá esperar de três a seis meses até a próxima visita. Porém, se houver necessidade de tratamento (fazer uma restauração, extrair um dente do siso ou restaurar a coroa de um dente) marque uma nova consulta. Não se esqueça de fazer todas as perguntas que tiver antes do término da consulta. Esta é a oportunidade para esclarecer qualquer dúvida que tenha.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
