<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <?php 
            $h1 = "Quem Somos";
            $title = "Quem Somos";
            $desc = "Encontre aqui as clinicas mais proximas para o seu tratamento odontologico. Consulta Ideal.Chegou a hora de você ter o sorriso que sempre sonhou, com o melhor"; 
            include("inc/head.php");
        ?>
    </head>
    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>

            <div class="container mb-md-0 mb-5">
                <div class="row">
                    <div class="col-12 text-black">
                        <p>Já pensou em ter acesso gratuito a uma ferramenta capaz de buscar os melhores consultórios odontológicos mais perto da sua região? O Consulta Ideal nasceu com essa missão.</p>

                        <p>Desenvolvido sob uma tecnologia de ponta, basta você acessar a plataforma, digitar o seu CEP e escolher a especialidade odontológica que você precisa. Vamos filtrar os melhores consultórios odontológicos, perto de você, em apenas um clique. Tenha acesso a endereço, telefone e também a distância da sua localização. Coloque a sua saúde bucal em primeiro lugar com o Consulta Ideal.
                        </p>
                    </div>
                </div>
            </div>
            <div style="margin-top:-50px;">
                <?php include("section1.php"); ?>
            </div>
            <?php include("inc/footer.php"); ?>
        </div>
    </body>
</html>
