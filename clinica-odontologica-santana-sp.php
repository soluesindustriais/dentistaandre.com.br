<?php
include('inc/vetKey.php');
$h1 = "clínica odontologica santana sp";
$title = $h1;
$desc = "Clínica odontologica santana sp As principais doenças bucais são causadas por descuidos com a higiene oral. A má escovação dentária aliada a uma";
$key = "clínica,odontologica,santana,sp";
$legendaImagem = "Foto ilustrativa de clínica odontologica santana sp";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Clínica odontologica santana sp</h2><p>As principais doenças bucais são causadas por descuidos com a higiene oral. A má escovação dentária aliada a uma alimentação não balanceada e rica em açúcares são as principais causas de problemas na boca, bochechas e gengivas. O maior motivo por uma procura de uma boa clínica odontologica santana sp.</p><p>As consequências da falta de higiene bucal vão muito além da perda de dentes. Isso porque problemas de saúde oral podem desencadear complicações no nosso organismo em geral. Mas, você sabe quais são as principais doenças bucais e como evitá-las? Tudo começa com uma clínica odontologica santana sp.</p><h2>Quais são as principais doenças tratadas em uma clínica odontologica santana sp?</h2><p align="center"></p><ul><li>Cárie: Essa é uma das principais doenças bucais e também uma das mais comuns em todo o mundo. A cárie origina-se da ação dos ácidos liberados pelas bactérias quando essas metabolizam os açúcares ingeridos. Esses ácidos são capazes de corroer e romper as estruturas calcificadas do dente (esmalte e dentina). É, portanto, um processo de desmineralização do dente;</li></ul><ul><li>Aftas: As aftas também constituem uma doença comum em todo o mundo. Elas são um tipo de úlcera bucal e formam uma pequena ferida esbranquiçada circular, rodeada por tecido avermelhado. Ocorrem na mucosa da boca e podem ser simples ou múltiplas, mas quase sempre dolorosas ou, no mínimo, causam desconforto. Podem durar até duas ou três semanas;</li></ul><ul><li>Eritema migratório: O eritema migratório é também conhecido como glossite migratória benigna ou, popularmente, como língua geográfica. Trata-se de um distúrbio benigno que aparece na parte dorsal (superior) da língua;</li></ul><ul><li>Halitose: A halitose, ou mau hálito também é uma das principais doenças bucais e atinge cerca de 40% da população mundial. O problema geralmente é causado pela falta de higiene adequada com os dentes e a língua. Mas também pode ser resultante de infecções bucais ou de maus hábitos, como consumo frequente de álcool e uso de tabaco;</li></ul><ul><li>Gengivite: A gengivite também é uma das principais doenças bucais, que pode prejudicar a beleza de seu sorriso. Ela é uma inflamação da gengiva e, de modo geral, é provocada pela presença de placa bacteriana acumulada sob os tecidos gengivais;</li></ul><ul><li>Periodontite: A periodontite, como visto, é a evolução da gengivite não tratada a tempo. Quando ela ocorre, os quadros infecciosos passam das gengivas para os ligamentos e ossos que dão suporte aos dentes. A perda desse suporte, por sua vez, faz com que os dentes fiquem moles e em casos graves, acabem caindo.</li></ul><h2>Como evitar o aparecimento dessas doenças?</h2><p align="center"></p><p>Assim como outras doenças que acometem nosso organismo, os problemas bucais podem ser evitados. E quanto antes você cuidar de seus dentes, melhor para você, pois é possível uma pessoa conservar sua dentição natural por toda a vida. A clínica odontologica santana sp pode ser a melhor opção de como manter seu sorriso bonito até a velhice.</p><p>Para manter seus dentes intactos, você deve escovar os dentes sempre após as refeições e ainda contar com o uso do fio dental. Enxaguantes também são bem-vindos, desde que usados com recomendação do dentista da clínica odontologica santana sp. A qualidade da escovação também conta e não apenas a quantidade. Por isso, você deve escolher os produtos certos para cuidar de seu sorriso. A escova de dentes deve ter cerdas macias e um formato anatômico que possibilite que ela alcance as áreas mais difíceis.</p><p>Todos os detalhes dos procedimentos podem ser tirados na primeira visita à clínica odontologica santana sp. Portanto, não perca tempo e marque sua consulta.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
