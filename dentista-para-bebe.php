<?php
include('inc/vetKey.php');
$h1 = "dentista para bebe";
$title = $h1;
$desc = "Dentista para bebe A odontopediatria ou popularmente conhecido como dentista para bebe, é uma área dentro da odontologia dedicada aos cuidados da saúde bucal";
$key = "dentista,para,bebe";
$legendaImagem = "Foto ilustrativa de dentista para bebe";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista para bebe</h2><p>A odontopediatria ou popularmente conhecido como dentista para bebe, é uma área dentro da odontologia dedicada aos cuidados da saúde bucal de bebês, crianças e, até, adolescentes. O principal foco dessa especialidade é promover a consciência sobre a importância de criar hábitos saudáveis da criança, para que sejam mantidos ao longo da vida.</p><p>Quando os pais incluem os bebes logo cedo em uma rotina de idas aos consultórios odontológicos, melhor sera a forma que o bebe irá reagir em contato com o dentista, desenvolvendo um aspecto de confiança. Um dos motivos de levar o filho o quanto antes ao consultório de um dentista, além de claro, evitar doenças e realizar algum procedimento é criar um vínculo entre o dentista e seu bebe, evitando assim, que ele cresça com aquele famoso medo de ir ao dentista, não suportando nem mesmo o barulho dos instrumentos.</p><p>Já para os pais, o dentista para bebe serve como um guia para percorrer e entender todas as fases da infância. Isso ocorre antes mesmo do nascimento do primeiro dentinho.</p><h2>Como o dentista para bebe pode ajudar?</h2><p align="center"></p><p>Quando falamos de dentista para bebe, estamos falando da odontopediatria, a área que atende diretamente crianças e adolescentes. Além de realizar tratamentos e procedimentos, esse especialista tem como características orientar bem os pais sobre os cuidados a serem tomados em casa. O desenvolvimento da arcada dentária muda na mesma velocidade em que a criança cresce, assim despertando ainda mais dúvidas nos pais. </p><p>O odontopediatra também acaba auxiliando os pais nessa fase, já que o profissional é capaz de avaliar a criança caso a caso, e assim, mostrar quando é o momento certo de fazer qualquer tipo de mudança no dia a dia da criança, para sua melhor saúde.</p><p>Depois de passar desta fase mais complicada, o profissional deve continuar dando total acompanhamento na fase da adolescência. Quanto maior o período de acompanhamento melhor será os benefícios na vida adulta.</p> <h2>Quais os problemas bucais são mais comuns em crianças?</h2><p align="center"></p><p>Quando a criança chega próximo à idade de 6 anos, os dentes de leite começam a nascer, fase essa que requer um bom acompanhamento do profissional. Essa primeira leva de dentes serve como uma espécie de guia para o desenvolvimento dos dentes fixos que vão nascer no futuro.</p><p>Os problemas bucais são mais comuns são:</p><ul><li>Chupetas;</li></ul><ul><li>Mamadeiras;</li></ul><ul><li>Cáries;</li></ul><ul><li>Bruxismo;</li></ul><ul><li>Mau hálito.</li></ul><h2>Não adie a primeira consulta com o dentista para bebe</h2><p align="center"></p><p>Ninguém melhor que o dentista para bebe para entender tudo sobre o que pode ocorrer com os dentinhos dos pequenos. Com diversos tipos de técnicas e aparelhos apropriados, o profissional está sempre preparado para cuidar de qualquer caso. Dando assim, uma garantir de saúde a boca de nossos bebes. </p><p>Ninguém melhor que o dentista para bebe para entender tudo sobre o que pode ocorrer com os dentinhos dos pequenos. Com diversos tipos de técnicas e aparelhos apropriados, o profissional está sempre preparado para cuidar de qualquer caso. Dando assim, uma garantir de saúde a boca de nossos bebes. </p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
