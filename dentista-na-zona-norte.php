<?php
include('inc/vetKey.php');
$h1 = "dentista na zona norte";
$title = $h1;
$desc = "Por que escolher por um dentista na zona norte? Para encontrar um bom dentista na zona norte, é sempre bom pedir referências para as pessoas em quem";
$key = "dentista,na,zona,norte";
$legendaImagem = "Foto ilustrativa de dentista na zona norte";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Por que escolher por um dentista na zona norte?</h2><p align="center"></p><p>Para encontrar um bom dentista na zona norte, é sempre bom pedir referências para as pessoas em quem você confia: seus amigos, membros de sua família, conhecidos, colegas de trabalho, seu farmacêutico ou o médico da família. Pergunte a eles sobre com que tipo de dentistas fazem o tratamento dentário, se é um dentista clínico geral ou especialista, há quanto tempo tratam com este profissional e como é o relacionamento. É importante que você escolha um dentista na zona norte com quem você se sinta seguro.</p><p>Para escolher bem seu dentista na zona norte, é útil também:</p><ul><li>Entrar em contato com associação de dentistas e solicitar uma lista dos profissionais recomendáveis;</li></ul><ul><li>Fazer uma busca bem detalhada na Internet. A cada dia aumenta o número de dentistas que têm sites onde explicam seus métodos de tratamento.</li></ul><h2>Que tipo de dentista na zona norte eu estou precisando?</h2><p>O dentista na zona norte com formação geral é treinado para fazer todo tipo de tratamento e podem, se for preciso, indicar um dos especialistas relacionados abaixo:</p><ul><li>Odontopediatra: dentista especializado em bebes, crianças e adolescentes;</li></ul><ul><li>Endodontista: dentista com principal função em tratamento de cáries e canais;</li></ul><ul><li>Protesista: dentista especializado em, próteses dentárias, conhecidas como dentaduras;</li></ul><ul><li>Cirurgião bucal: dentista responsável por remover cistos, tumores e dentes. Com intuito de corrigir fraturas ou outros procedimentos cirúrgicos;</li></ul><ul><li>Ortodontista: dentista especialista na posição dos dentes, corrigidos por meio de aparelhos ortodônticos;</li></ul><ul><li>Periodontista: dentista especializado em doenças da gengiva.</li></ul><h2>A importância da odontologia em nossas vidas</h2><p align="center"></p><p>A saúde de sua boca é uma parte de suma importância para o seu corpo como um todo. Por isso, você deve ter os cuidados especiais com ela. A saúde bucal tem aspecto direto na vida de um ser humano, podendo garantir melhor qualidade se tratada como deve ser. Indo muito mais além da parte estética e confiança.</p><p>No caso da perda dos dentes, o resultado é direto na limitação da mastigação, por exemplo. A mastigação mal feita afeta diretamente o processo digestivo do ser humano, já que, se os alimentos não são mastigados de forma correta, eles também não são processados da maneira correta em nosso organismo.</p><p>O dentista na zona norte tem como principal intuito melhorar a saúde bucal do paciente, vindo desde a parte da higienização até mesmo a fatores mais complexos, como procedimentos cirúrgicos. É sempre essencial manter uma rotina frequente de idas ao dentista, mesmo que seja apenas para simples consultas.</p><p>Ele ainda pedirá exames radiográficos e realizará a profilaxia dentária. Esta, nada mais é do que o processo de limpeza e conservação dos dentes para evitar problemas futuros. Assim, além de auxiliar na prevenção, a odontologia preventiva também permite manter os dentes e gengivas saudáveis.</p><p>São diversos aspectos que podemos sitar para demonstrar a importância de termos a odontologia em nossas vidas, a saúde e a estética andam lado a lado nesse processo. Quanto temos a saúde bucal 100%, temos também uma boca visivelmente limpa e com uma estética impecável.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
