<?php
include('inc/vetKey.php');
$h1 = "dentista lente de contato";
$title = $h1;
$desc = "Dentista lente de contato O dentista lente de contato realiza o trabalho estético onde se cobre apenas a frente do dente, chamada região vestibular. O";
$key = "dentista,lente,de,contato";
$legendaImagem = "Foto ilustrativa de dentista lente de contato";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista lente de contato</h2><p>O dentista lente de contato realiza o trabalho estético onde se cobre apenas a frente do dente, chamada região vestibular. O material que a lente é feita é o dessilicato de lítio, material esse que costuma ser mais resistente que outros usados em procedimentos odontológicos, como porcelanas.</p><p>O procedimento de lente de contato vem crescendo e se modificando cada vez mais, devido aos avanços nos estudos odontológicos, os materiais utilizados para o tratamento estão cada vez mais seguros e duradouros, sem causar danos na boca de quem o usa. Essa nova fase das lentes de contato agora, conta com o material feito em dessilicato de lítio, material resistente e eficaz em seu processo.</p><p></p><h2>Indicação para o uso das lentes de contato dental</h2><p align="center"></p><p>As lentes de contato dentais são indicadas para pacientes que têm dentes com:</p><ul><li>Formato inadequado;</li></ul><ul><li>Tamanho irregular ou para correção da posição dos dentes;</li></ul><ul><li>Dentes escurecidos ou com manchas;</li></ul><ul><li>Dentes desgastados ou com pequenas fraturas;</li></ul><ul><li>Dentes ligeiramente desalinhados ou com forma irregular.</li></ul><p>Analisando os tipos de uso da lente de contato, vemos que elas são indicadas para grande parte dos procedimentos estéticos feitos em um consultório odontológico. Impossibilitando grande parte do desgaste tanto do dente quanto do material em si. Uma mudança significativa, já que no passado o desgaste era muito comum entre os dentes, podendo ser tão grande que o dentista lente de contato indicava até mesmo um rápido tratamento de canal. Evitando assim, qualquer tipo de risco do procedimento com as lentes serem perdidos.</p><h2>Como é o processo para a colocação das lentes de contato dentárias?</h2><p align="center"></p><p>Todo procedimento dentário requer cuidado e muita atenção por parte do dentista, o processo de implantação da lente de contato não é diferente, para o paciente é algo muito simples, mas para o cirurgião requer cuidado dobrado. O encaixe da lente nos dentes precisar ser totalmente perfeito, para que não altere os próximos encaixes que são colocados ao lado.</p><p>Para o processo geral, são necessárias diversas sessões ao dentista lente de contato, geralmente na primeira é apenas para elaborar o procedimento e tirar qualquer tipo de dúvida junto ao paciente. Após isso, o paciente retornará para fazer a prova do molde dos dentes de dessilicato de lítio, sendo uma etapa importante, para ver se o encaixe em sua boca está totalmente adequado. </p><p>Por fim, o paciente pode tirar a prova de como esta a cor de seus dentes, caso esteja mais claro que do ele optou, o dentista pode mudar, e deixar o mais natural possível, já que as lentes ainda não foram instaladas, tem tempo para mudar. Mas há alguns casos em que não tem como fazer essa mudança, caso venha totalmente pronto para instalação.</p><p>Após todos essas etapas resolvidas, o dentista lente de contato irá realizar as mudanças necessárias, e em seguidas as lentes serão colocadas. Para realizar um bom procedimento do começo ao fim, é sempre bom poder contar com um profissional a altura, capaz de tornar o tratamento totalmente eficaz, um dentista lente de contato de qualidade.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
