<?php
include('inc/vetKey.php');
$h1 = "clínicas odontológicas";
$title = $h1;
$desc = "Clínicas odontológicas Muitas pessoas evitam ir a clínicas odontológicas, pois dizem ter medo, aflição ou simplesmente por que não gostam. Porém,";
$key = "clínicas,odontológicas";
$legendaImagem = "Foto ilustrativa de clínicas odontológicas";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Clínicas odontológicas</h2><p>Muitas pessoas evitam ir à clínicas odontológicas, pois dizem ter medo, aflição ou simplesmente por que não gostam. Porém, nossos dentes e boca merecem muita atenção. É importante ressaltar que os aparatos odontológicos evoluíram muito nos últimos anos, o que tende a minimizar os desconfortos e dores do paciente.</p><p>Consultas rápidas e simples podem evitar uma série de problemas, veja alguns motivos para você deixar o medo de lado e fazer uma visita ao seu dentista.</p><p>É muito comum encontrar pessoas que acham que, por não estarem sentindo dor ou incômodos nos dentes, está tudo bem. Algumas vezes, mesmo em consultas rotineiras onde o paciente não apresenta queixa, são encontradas desordens bucais como cáries, acúmulo de tártaro e muitos outros problemas.</p><p>Portanto, é somente através do exame clínico que o dentista pode, de fato, analisar a saúde bucal do paciente. Ir ao dentista de maneira regular previne cáries e outras doenças periodontais, evitando assim problemas futuros. Visitas regulares são, sem dúvida, a melhor forma de prevenção.</p><h2>Como clínicas odontológicas podem melhorar a autoestima</h2><p align="center"></p><p>Muitas pessoas deixam de sorrir ou abrem menos a boca por sentirem vergonha de seus dentes serem tortos, quebrados, ou até mesmo amarelados. O mesmo pode acontecer com quem tem mau hálito. Pensando assim, ir ao dentista é uma maneira de prevenir a saúde bucal e também de recuperar a autoestima. Quem está em dia com a saúde bucal, ri com mais facilidade, conversa mais e, consequentemente, se diverte mais.</p><p>Ir à clínicas odontológicas regularmente é uma forma de cuidado com os dentes e a saúde, portanto, é uma prática que não deve ser negligenciada de maneira alguma. Não visite seu dentista apenas quando sentir dor, esteja presente em seu consultório ao menos duas vezes por ano a fim de conferir como anda sua saúde bucal, afinal, o maior beneficiado nessa história será você mesmo.</p><p>Além da autoestima, manter uma rotina saudável ao ir à clínicas odontológicas pode ajudar em outros pontos, tais como:</p><ul><li>Prevenir doenças;</li></ul><ul><li>Manter o sorriso claro e limpo;</li></ul><ul><li>Perder o medo de dentista;</li></ul><ul><li>Evitar a dor de dente;</li></ul><ul><li>Evitar o mau hálito.</li></ul><h2>Quem vai ganhar é sua saúde quando você visitar seu dentista regularmente</h2><p align="center"></p><p>Muitos perguntam porque se deve voltar ao dentista de seis em seis meses. As pessoas tendem a procurar o dentista somente em caso de dor extrema ou para solucionar algum problema de estética bucal, como por exemplo, um dente quebrado ou lascado. O que poucas pessoas se atentam no final das contas, é que os dentes merecem e necessitam de muita atenção e você precisa visitar seu dentista regularmente. Infelizmente, a maioria das pessoas deixam de se consultar com seu dentista periodicamente para manutenção ou revisão, pior ainda, acham que estas consultas não são necessárias.</p><p>Muitos pacientes acham que estas consultas de controle nas clínicas odontológicas não são necessárias e acabam passando anos sem retornar ao dentista. Mas existem diversos motivos, já vistos, pelos quais você deve visitar seu dentista regularmente, pelo menos a cada seis meses para uma consulta com seu dentista, mesmo após o término de seu tratamento clínico ou procedimento estético.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
