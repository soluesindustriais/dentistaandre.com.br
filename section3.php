<section class="video-section section-default">   
    <div class="h-100 w-100 vcont">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center"> 

                    <h3 class="text-white mb-2 text-uppercase h2">Implantes</h3>
                    <p class="text-white h4 mb-4 font-weight-light">Os procedimentos de implantes estão cada vez mais usando a tecnologia ao seu favor. Um dos grandes problemas dos brasileiros, a perda de dentes pode ser resolvida com a ajuda de implantes modernos feito por profissionais qualificados. Encontre os melhores profissionais fazendo uma pesquisa pelo Consulta Ideal.</p>
                    <a href="<?= $url; ?>informacoes" class="btn btn-lg theme-btn">Saiba mais</a>  

                </div>
            </div>
        </div>
    </div>
</section>