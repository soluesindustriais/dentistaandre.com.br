<?php
include('inc/vetKey.php');
$h1 = "clínica odontológica em são paulo";
$title = $h1;
$desc = "Clínica odontológica em são paulo A clínica odontológica em são paulo é um espaço que só é lembrado pela maioria das pessoas quando bate aquela";
$key = "clínica,odontológica,em,são,paulo";
$legendaImagem = "Foto ilustrativa de clínica odontológica em são paulo";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Clínica odontológica em são paulo</h2><p>A clínica odontológica em são paulo é um espaço que só é lembrado pela maioria das pessoas quando bate aquela dor de dente. Ou, se não, no dia do dentista, por amigos e pacientes. Se tudo estiver bem e nenhuma dor estiver incomodando, quase ninguém se lembra de visitar o dentista para prevenção e manutenção periódica. Por isso, quando aparecem na clínica odontológica em são paulo, o problema já está em estado grave.</p><p>Cuidar da saúde bucal é um cuidado necessário para todas as pessoas, de todas as faixas etárias, sem exceção. Isso porque em cada fase da vida, os dentes precisam de cuidados especiais, de modo a garantir a saúde bucal e a preservação dos dentes em bom estado para o resto da vida.</p><p>Ficar muito tempo sem ir ao dentista é um perigo e pode trazer sérias consequências, pois com o decorrer do tempo, problemas pequenos e fáceis de serem resolvidos se tornam grandes e de difíceis resoluções, que além de demandarem mais tempo de tratamento, causam mais incômodos e podem resultar em problemas graves.</p><h2>Uma clínica odontológica em são paulo, ajuda em todos os momentos</h2><p align="center"></p><p>Para os profissionais da área da odontologia, a importância das visitas periódicas já é clara. No entanto, como passar essas informações para seus pacientes e fazer com que eles entendam que a sua clínica não precisa ser procurada apenas em casos urgentes, ou quando uma dor começar a incomodar?</p><p>Esse é um desafio enfrentado pela maioria dos dentistas, porque, por mais que se fale durante as consultas, os pacientes só procuram quando precisam. Você também sofre com isso? Aquele paciente que você nem lembrava mais aparece depois de um ou dois anos com um enorme problema para você resolver?</p><h2>Ações usadas por Clínica odontológica em são paulo para conscientizar os pacientes</h2><p align="center"></p><p>Grandes clínicas odontológicas espalhadas pelo Brasil, fazem diversas ações para conscientizar as pessoas da importância de manter um habito saudável em relação a sua boca. Veja algumas dessas ações:</p><ul><li>Comunicação;</li></ul><ul><li>Forneça materiais informativos;</li></ul><ul><li>Aproxime-se de seus pacientes;</li></ul><ul><li>Ofereça planos especiais e descontos.</li></ul><p>O grande erro que muitos dentistas cometem é não manter uma boa e clara comunicação com seus pacientes. A comunicação, ou seja, a conversa é o principal meio pelo qual você pode realizar a conscientização, mostrando o porquê as visitas periódicas são importantes.</p><p>Uma boa comunicação, portanto, não significa que apenas dizer para o seu paciente que ele precisa voltar com mais frequência já basta. Pelo contrário, uma boa comunicação consiste em uma conversa baseada em informações. Isso quer dizer que você precisa esclarecer, de modo muito didático, quais as consequências que ele poderá sofrer se passar muito tempo sem realizar uma consulta.</p><p>Por incrível que pareça, muitas pessoas ainda sentem medo de ir ao dentista. Por isso, é cada vez mais difícil convencê-las de realizar as visitas frequentes a clínica odontológica em são paulo. Se o medo for o problema, isso está fácil de ser resolvido, uma vez que o medo é o resultado da falta de informação.</p><p>Sendo assim, se você fornecer as informações necessárias, explicando como se dão os procedimentos, é bem mais provável que o seu paciente medroso se sinta mais à vontade e mais confiante para realizar consultas preventivas com mais frequência.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
