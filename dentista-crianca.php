<?php
include('inc/vetKey.php');
$h1 = "dentista criança";
$title = $h1;
$desc = "Dentista criança A primeira ida ao dentista criança deve acontecer assim que começam a nascer os primeiros dentinhos, dessa forma o dentista";
$key = "dentista,criança";
$legendaImagem = "Foto ilustrativa de dentista criança";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista criança</h2><p align="center"></p><p>A primeira ida ao dentista criança deve acontecer assim que começam a nascer os primeiros dentinhos, dessa forma o dentista consegue fazer a avaliação da dentição e da saúde bucal sem precisar “esperar” por algum problema, mas sim como prevenção.</p><p>Mas algumas crianças podem apresentar algum tipo de medo ou ansiedade no consultório do dentista. E muitas vezes o medo dos pais acaba sendo refletido nos pequenos que, mesmo sem experiências negativas com dentistas, reproduzem o comportamento. Em algumas situações os pais acabam colocando medo nas crianças ao falarem antecipadamente sobre os tratamentos, por exemplo. E, então, um tratamento tranquilo acaba gerando uma ansiedade na criança. Por isso, o ideal é que pais e dentistas saibam como ajudar crianças que têm medo.</p><ul><li>O uso de mamadeira;</li></ul><ul><li>Dá chupeta;</li></ul><ul><li>Amamentação;</li></ul><ul><li>Hábitos alimentares;</li></ul><ul><li>Chupar o dedo;</li></ul><ul><li>Bruxismo noturno.</li></ul><p>Esses são somente alguns assuntos de extrema importância para os pais conversarem com o dentista criança.</p><h2>O que fazer quando seu filho tem medo do dentista criança?</h2><p align="center"></p><p>Cada dentista tem seu método para ajudar ou distrair a criança em momentos de tensão. Utilizar o mundo do faz de conta, da imaginação aliados aos personagens prediletos de cada paciente. Assim, o contato fica mais próximo e estabelece uma confiança. Outro recurso usado pela profissional é de mostrar e explicar para a criança qual será o procedimento antes de realizá-lo. Em geral, profissionais especializados em crianças também têm atrativos no consultório, assim o foco da criança fica no lúdico do ambiente.</p><p>Já do lado dos pais, o ideal é que transmitam segurança e tranquilidade para as crianças. Os pais devem tratar o dentista como um amigo que vai ajudar os dentes a ficarem cada vez mais fortes e saudáveis, e não fazer ameaças de dor ou de tratamentos traumáticos. Os pais também podem explicar que o dentista, assim como o pediatra, é alguém de confiança e que ela não precisa se preocupar, porque aquela é uma consulta normal.</p><h2>Qual a idade certa de levar meu filho ao dentista criança?</h2><p align="center"></p><p>É importante levar seu filho ao dentista criança desde cedo. Assim, alguns problemas podem ser evitados e seu filho não terá grandes traumas no futuro. É importante dividir as fases de dentição. Para cada uma delas, há necessidades específicas.</p><ul><li>Bebê: Normalmente, o primeiro dente surge aos 6 meses de vida. Não se preocupe se, com o seu filho, isso só acontecer com 1 ano de idade, também é considerado normal. Neste momento, o dentista mais orientará os pais a como higienizar da melhor forma a região e, também, poderá indicar algumas soluções para que o bebê sofra menos nesta fase de erupção dos dentes;</li><li>Criança de 2 a 3 anos: Nesta fase, a criança já pode apresentar os 20 dentes de leite em sua boca (quantidade menor que os dentes permanentes que, normalmente, são 32 na fase posterior);</li><li>Criança com 6 anos: Nesta idade, costumam aparecer os primeiros dentes permanentes, sendo que os de leite começam a “amolecer”. O dentista criança pode ajudá-lo a cuidar destes novos dentes e intervir, caso haja algum desenvolvimento fora da normalidade.</li></ul><p>Vale a pena ter um dentista de confiança desde o surgimento dos primeiros dentes do bebê. Ele poderá te indicar que frequência você deverá ter com as visitas ao seu consultório.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
