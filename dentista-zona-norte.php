<?php
include('inc/vetKey.php');
$h1 = "dentista zona norte";
$title = $h1;
$desc = "Dentista zona norte, ajuda a evitar doenças Com certeza você já escutou a frase “a prevenção é o melhor remédio”. Ela se encaixa perfeitamente quando";
$key = "dentista,zona,norte";
$legendaImagem = "Foto ilustrativa de dentista zona norte";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista zona norte, ajuda a evitar doenças</h2><p align="center"></p><p>Com certeza você já escutou a frase “a prevenção é o melhor remédio”. Ela se encaixa perfeitamente quando falamos sobre odontologia e a importância de ter um acompanhamento em um dentista zona norte. Para ressaltar a importância desse especialista, veja algumas dicas para você cuidar melhor do seu sorriso.</p><p>É essencial fazer visitas regulares no mínimo a cada seis meses ao dentista zona norte, pois é ele quem vai identificar os principais problemas relacionados com a saúde dos dentes e da boca. Em uma consulta regular, o profissional avaliará a higienização, identificando se existem cáries e problemas nas gengivas. O dentista zona norte também é quem verifica a mordida e uma possível necessidade estética.</p><h2>Benefícios de fazer a limpeza dentária com frequência</h2><p align="center"></p><p>Todos sabem que ter uma boca limpa é essencial para a saúde e bem-estar. Manter sua boca totalmente higienizada com uma boa limpeza dentária por um dentista zona norte é um dos procedimentos mais seguros a se fazer. Veja quais benefícios a limpeza pode trazer:</p><ul><li>Deixa menor o risco de cáries;</li></ul><p>Uma boa limpeza dentária realizada por um bom dentista zona norte, consiste no procedimento de raspagem de qualquer camada de tártaro existente sobre os dentes. Diminuir as chances de cáries também resultam na, não aparição de canais.</p><ul><li>Dificulta o surgimento de doenças bucais;</li></ul><p>Além de acabar com a placa bacteriana, assim diminuindo os riscos de cáries, a limpeza dentária não deixa que nenhuma doença fica mais agravante, como no caso de doenças na gengiva, ou até mesmo a periodontite, afetando diretamente a arcada dentária.</p><ul><li>Mantém um hálito saudável e refrescante.</li></ul><p>A limpeza dentária feita com frequência, em uma boa clínica, mantém o hálito fresco e saudável, trazendo a sensação de como se tivesse acabado de escovar os dentes.</p><h2>Evite problemas futuros</h2><p align="center"></p><p>As doenças bucais são silenciosas, e demoram até para apresentar qualquer tipo de sintoma, isto só acontece quando a doença está mais evoluída.  Por conta disso, é essencial que a visita ao dentista zona norte seja com frequência. Evitando que o problema só seja descoberto mais tarde do que se deve.</p><p>Um bom convênio odontológico pode ajudar a evitar problemas bucais, já que ele inclui:</p><ul><li>Atendimento de urgência 24 horas;</li></ul><ul><li>Serviços de orientação de higiene bucal;</li></ul><ul><li>Profilaxia e limpeza;</li></ul><ul><li>Remoção de tártaros;</li></ul><ul><li>Aplicação de flúor.</li></ul><p>Além disso, você também poderá dispor de extrações, ortodontistas, radiografias e odontopediatria, para garantir a saúde bucal de toda a família.</p><h2>A boa e velha escovação</h2><p align="center"></p><p>A base de tudo é a escovação, realizada pelo menos três vezes ao dia, junto com o fio dental e também um enxaguante bucal de qualidade. O jeito mais apropriado de começar uma escovação é pelo lado que se morde, depois do lado de dentro dos dentes e para finalizar na parte da frente. Além disso, manter a saúde bucal inclui também o consumo de água em uma quantidade adequada para se manter sempre hidratado, ter uma dieta com frutas e verduras e controlar o consumo de açúcar e de farinha.</p><p>Essas dicas juntas a uma boa escovação dental, traz vantagens a saúde bucal e também para o lado estético.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
