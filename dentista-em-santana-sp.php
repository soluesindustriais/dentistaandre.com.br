<?php
include('inc/vetKey.php');
$h1 = "dentista em santana sp";
$title = $h1;
$desc = "Dentista em santana sp Visitar o dentista em santana sp é uma tarefa que precisa estar sempre em nosso calendário, ainda mais para os moradores";
$key = "dentista,em,santana,sp";
$legendaImagem = "Foto ilustrativa de dentista em santana sp";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista em santana sp</h2><p align="center"></p><p>Visitar o dentista em santana sp é uma tarefa que precisa estar sempre em nosso calendário, ainda mais para os moradores da região. Além de ser uma boa oportunidade para rever alguns hábitos higiênicos e alimentares é o melhor momento para prevenir e identificar precocemente aquelas doenças que insistem em tomar conta dos dentes. Mas a questão é, com que frequência devo realizar essa visita?</p><p>Se o assunto é o bem-estar do nosso corpo precisamos estar sempre em dia com os cuidados e visitar o médico de forma regular para fazer revisões. O mesmo acontece com a nossa saúde bucal. É importante que a frequência das consultas vão depender apenas da necessidade de cada paciente. Um intervalo máximo de ida ao dentista em santana sp é de seis meses é o recomendado, porém pode ser indicado quatro em quatro meses para pacientes com doença periodontal crônica e crianças em controle de atividade de cárie, por exemplo.</p><h2>O que é feito nessas consultas ao dentista em santana sp?</h2><p align="center"></p><p>Com as ferramentas certas o dentista em santana sp irá investigar todo universo bucal do paciente. O cirurgião-dentista irá proceder à inspeção clínica da integridade, nos seguintes locais:</p><ul><li>Dentes;</li></ul><ul><li>Saúde gengival;</li></ul><ul><li>Busca por lesões suspeitas em região de mucosa;</li></ul><ul><li>Língua;</li></ul><ul><li>Assoalho de boca.</li></ul><p>Para aqueles que já são acompanhados com mais frequência o procedimento é mais objetivo. É necessário que se faça apenas a profilaxia profissional, para remoção de manchas extrínsecas do esmalte, e raspagem periodontal, caso seja necessária a eliminação de cálculos supra/subgengivais.</p><p>Sabemos que a prevenção de doenças é a função primordial de todo check-up, mas o dentista em santana sp também está lá para tirar dúvidas dos pacientes e atualizá-los sobre os hábitos higiênicos e alimentares. Instruções de dieta e higiene serão indicadas conforme a necessidade específica de cada paciente. Podemos ainda identificar a necessidade de tratamento ortodôntico ou estético.</p><h2>O tratamento do profissional continua em casa</h2><p align="center"></p><p>Tirando as crianças que precisam do apoio dos responsáveis para realizarem os cuidados pessoais, o paciente após uma certa idade precisa ser independente com relação a sua saúde bucal. Isso quer dizer que o dentista ensina o passo a passo da higiene e a pessoa precisa segui-los para que nenhuma doença se instale em seu sorriso. Normalmente são passos simples: Após a refeição ou ingestão de líquidos doces e ácidos (incluindo xaropes, no caso das crianças) deve-se escovar os dentes e fazer uso correto do fio dental.</p><h2>A escova perfeita para o seu sorriso</h2><p align="center"></p><p>Existem escovas de formatos variados para cada caso que facilitam o controle de placa bacteriana, alertam dentista em santana sp. Uma ferramenta que auxilia muito é a escova elétrica. Com cabeça arredondada e cerdas macias que fazem diversos movimentos por segundo, ela pode deixar todo o trabalho da escovação ainda mais fácil e manter seus dentes limpos em apenas dois minutos.</p><p>Não tem mistério algum, seguindo todas essas recomendações você terá dias sempre sorridentes. Está esperando o que para marcar sua próxima consulta com seu dentista em santana sp?</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
