<?php
include('inc/vetKey.php');
$h1 = "dentista de emergência";
$title = $h1;
$desc = "Dentista de emergência Dentista de emergência, como o próprio nome diz trata-se de uma situação normalmente imprevista e que exige cuidados para";
$key = "dentista,de,emergência";
$legendaImagem = "Foto ilustrativa de dentista de emergência";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista de emergência</h2><p align="center"></p><p>Dentista de emergência, como o próprio nome diz trata-se de uma situação normalmente imprevista e que exige cuidados para situações onde algumas vezes as ocorrências podem ser críticas. Digamos que você tem uma dor de dente incessante ou sofre algum acidente durante aquela festa e precisa de um atendimento às 20 horas de um sábado à noite, a quem recorrer? O dentista de emergência é a solução nestes casos, o serviço é oferecido em formato 24 horas para que sua saúde esteja sempre em primeiro lugar.</p><p>E todos sabemos que é necessário buscar sempre um local apropriado com profissionais de qualidade que ofereçam segurança e garantia em seu atendimento, uma clínica odontológica moderna, capacitada com as mais avançadas tecnologias e atendendo a diversas especialidades odontológicas.</p><p>Quando falamos que o funcionamento é emergencial, significa que o atendimento é feito 24 horas por dia, não importando o problema. Será possível fazer um atendimento independentemente do nível da emergência.</p><h2>Diferenciais do dentista de emergência</h2><p align="center"></p><p>Na grande maioria dos casos emergenciais, a pessoa normalmente está com algum acompanhante então a empresa precisa estar apta para atender e solucionar o problema odontológico e também oferecer conforto a quem espera, por isso alguns diferenciais são de extrema importância. Mais segurança e tranquilidade para completar o seu bem-estar.</p><ul><li>Uma ampla estrutura para atender os pacientes e seus acompanhantes com conforto, com salas de espera confortáveis que contam com Wi-fi, TV e café. E salas de atendimento amplas e acessíveis, sempre limpas, com materiais devidamente esterilizados e embalados, além de contar com geradores de energia em caso de alguma queda de fornecimento;</li></ul><ul><li>Equipamentos modernos e com tecnologia de ponta na área odontológica para prestar o que há de melhor em solução para seu diagnóstico e tratamento;</li></ul><ul><li>Qualidade de atendimento que visa facilitar a comunicação e agilizar o tão importante atendimento do dentista de emergência, possibilitando oferecer sempre o melhor;</li></ul><ul><li>Profissionais altamente capacitados, conta com equipe multidisciplinar com especialistas em diversas áreas odontológicas, compromissados a estarem sempre atualizados;</li></ul><ul><li>Profissionais devidamente credenciados e registrados no conselho com suas especialidades cadastradas, a fim de dar total segurança sobre o atendimento prestado e conhecimento sobre a problemática, uma vez que são regulamentados e seguem todas as devidas regras e instruções necessárias para atendimentos e procedimentos.</li></ul><h2>Qual é a diferença entre emergência e urgência em odontologia?</h2><p align="center"></p><p>As palavras dentista de emergência e urgência são muito parecidas à primeira vista, mas em saúde são extremamente diferentes. Emergência é uma situação crítica ou um perigo iminente onde ocorre risco de vida.</p><p>Entretanto a Urgência e uma situação que deve ser resolvida imediatamente, que não pode ser adiada, mas que não tem risco de vida iminente. Na odontologia o que vemos mais são as urgências, principalmente em casos de doenças que afetam a polpa dos dentes e que provocam uma dor aguda muito difícil de suportar.</p><p>Nestes casos um diagnóstico rápido, feito por profissional competente, resolve a dor com a abertura do elemento dentário e extração da polpa. O aumento da expectativa de vida traz ao consultório odontológico indivíduos diabéticos, hipertensos, cardiopatas, asmáticos ou portadores de desordens hepáticas e renais, obrigando o profissional a adotar certas precauções antes de iniciar o tratamento propriamente dito.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
