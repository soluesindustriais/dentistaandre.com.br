<!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <?php 
            $h1 = "Informações";
            $title = "informacoes";
            $desc = "Veja informações sobre as clinicas mais proximas para o seu tratamento odontologico - Consulta Ideal.Chegou a hora de você ter o sorriso que sempre sonhou"; 
            include("inc/head.php"); ?>     
        </head>
        <body>
            <div class="site-wrap">
            
                <?php include("inc/header.php"); ?>

    <div class="container mb-4">
        <div class="row"  id='paginacao'>
          

                
       
                <?php       include_once('inc/vetKey.php');
            foreach ($vetKey as $key => $value) { ?>
                        <div class="col-lg-3 col-md-6 mb-4">
                            <div class="col-12 post-entry p-0  bg-white" style="box-shadow: 0 0px 10px rgba(0, 0, 0, 0.25);">
                                <a href="<?=$url.$value["url"];?>" class="d-block">
                                    <img src="<?=$url?>images/img-mpi/350x350/<?=$value['url']?>-1.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>">
                                </a>            
                                <h3 class="p-3 m-0 text-uppercase text-center d-flex justify-content-center align-items-center" style="min-height:76px"><a href="<?=$url.$value["url"];?>"><?=$value['key']?></a></h3>
                            </div>
                        </div>

                <?php } ?>
               </div>
        </div>
            
  

                <?php include("inc/footer.php"); ?>

            </div>    
        </body>
</html>