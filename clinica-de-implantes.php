<?php
include('inc/vetKey.php');
$h1 = "clínica de implantes";
$title = $h1;
$desc = "Clínica de implantes Uma clínica de implantes é responsável por procedimentos que ajudam muita gente a voltar a sorrir. Com eles, o paciente";
$key = "clínica,de,implantes";
$legendaImagem = "Foto ilustrativa de clínica de implantes";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Clínica de implantes</h2><p align="center"></p><p>Uma clínica de implantes é responsável por procedimentos que ajudam muita gente a voltar a sorrir. Com eles, o paciente pode levar uma vida normal e sem se preocupar com problemas de mastigação ou má-formação trazidos pela falta dentária. Mas o que algumas pessoas não sabem ou têm dúvidas é a real necessidade de fazer, quais são os tipos existentes e quanto tempo pode durar esse novo sorriso.</p><p>A clínica de implantes e outros processos revolucionaram o tratamento de reabilitação dentária e permitiram a milhares de pessoas a possibilidade de voltar a sorrir sem medo. Aliando a um sorriso saudável, a nova geração de implantes concilia o melhor da tecnologia às técnicas que se diferenciam e influenciam diretamente no resultado, bem como a satisfação do paciente.</p><h2>Tipos de implantes dentários feitos em uma clínica de implantes</h2><p align="center"></p><p>A implantodontia passa por um desenvolvimento, que se reflete na melhoria dos tratamentos e do resultado estético. A matéria-prima do implante é o titânio, já consagrado há mais de 40 anos, nos quesitos eficácia e segurança. Outra alternativa, porém recente, os primeiros implantes à base de zircônia desembarcaram no país com a promessa de algumas vantagens, por exemplo, a de anular os riscos de alergia e toxicidade por ser isento de metal.</p><p>Dos casos mais simples aos mais complexos, essa nova tecnologia surpreende o paciente e pode proporcionar um tratamento mais seguro, rápido, eficaz e com maior estabilidade nos implantes. O sucesso dessa técnica está compreendido basicamente com o fato de a substância ser biologicamente compatível com o osso natural do corpo humano e funcionar como um catalisador para a formação de uma nova estrutura óssea.</p><p>Um sistema de trabalho eficiente aliado à matéria-prima de qualidade bem escolhida por um profissional qualificado proporciona um resultado estético de excelência nas reabilitações orais do paciente.</p><p>Ir a uma clínica de implantes pode proporcionar ótimos benefícios ao paciente, tais como:</p><ul><li>Devolver a mastigação adequada;</li></ul><ul><li>Eliminar a dentadura;</li></ul><ul><li>Uma melhor higienização da boca;</li></ul><ul><li>Autoestima;</li></ul><ul><li>Sorriso natural;</li></ul><ul><li>Entre muitos outros.</li></ul><h2>O que é preciso para colocar um implante?</h2><p align="center"></p><p>Em uma clínica de implantes, o primeiro passo é o paciente passar por uma avaliação com um dentista de sua confiança. O profissional vai pedir alguns exames de imagem e de sangue para a parte cirúrgica. O próximo passo é a cirurgia, feita de forma tranquila em duas etapas. Primeiro, é colocado o implante. Neste meio, tem um processo de cicatrização que dura de 4 a 6 meses, dependendo do lugar que colocou, e depois é inserida a prótese.</p><p>Existem as próteses fixas e as móveis. Entre as fixas existem as facetas, lentes de contato que os artistas colocam hoje em dia. Já as móveis podem ser parcial, para substituir um ou dois dentes, ou total para substituir todos os dentes. Lembre-se que só o cirurgião-dentista pode decidir o melhor tipo de técnica para você, junto a uma boa clínica de implantes.</p><h2>Quais são os cuidados pós-cirúrgicos?</h2><p align="center"></p><p>Os cuidados que se estendem após os procedimentos cirúrgicos, são fundamentais para uma recuperação ideal. Inchaços e sangramentos são comuns, mas facilmente controlados. Para a alimentação também são exigidos cuidados seguindo sempre a orientação indicada. A higiene bucal também é essencial.</p><p>Quer saber tudo sobre implantes dentários? Então agende sua avaliação em uma boa clínica de implantes e se informe sobre as vantagens e benefícios de ter um implante.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
