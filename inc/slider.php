<?php if(!$isMobile): ?>
<div id="carousel" class="carousel slide" data-ride="carousel">    
    <div class="carousel-inner">
        <div class="carousel-item active one">	
            <div class="d-flex align-items-center w-100 h-100" style="background:rgba(0,0,0,.1);">
                <div class="container azul">
                    <div class="row justify-content-center ">
                        <div class="col-md-7 col-9 text-center slider_text">
                            <h2>Qual método de clareamento devo escolher?</h2>
                            <a href="<?=$url?>dentista-clareamento-dental" class="btn btn-lg theme-btn">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item two">	
            <div class="d-flex align-items-center w-100 h-100" style="background:rgba(0,0,0,.1);">
                <div class="container azul">
                    <div class="row justify-content-center ">
                        <div class="col-md-7 col-9 text-center slider_text">
                            <h2>Tratamentos e as principais prevenções</h2>
                            <a href="<?=$url?>consultorio-odontologico" class="btn btn-lg theme-btn">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="carousel-item three">	
            <div class="d-flex align-items-center w-100 h-100" style="background:rgba(0,0,0,.1);">
                <div class="container azul">
                    <div class="row justify-content-center ">
                        <div class="col-md-7 col-9 text-center slider_text">
                            <h2>Tenha um acompanhamento odontológico</h2>
                            <a href="<?=$url?>clinica-dental" class="btn btn-lg theme-btn">Saiba mais</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

</div>
    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<?php endif; ?>


