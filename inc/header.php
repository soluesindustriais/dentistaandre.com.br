<div class="d-none"   id="topo"></div>
<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close">
            <span class="icon-close2 js-menu-toggle"><i class="fas fa-times"></i></span>
        </div>
    </div>
    <div class="site-mobile-menu-body"></div>
</div>


<div class="sticky-top">      
    <div class="site-navbar">
        <div class="container py-1">
            <div class="row align-items-center">
                <div class="col-8 col-md-2">
                    <a href=""> <img class="site-logo" src="<?= $url; ?>images/logo.png" alt="Consulta Ideal"/></a>
                </div>
                <div class="col-md-10 col-4">
                    <nav class="site-navigation text-right">
                        <div class="container">
                            <div class="d-inline-block d-lg-none ml-md-0 mr-auto"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu fa fa-bars"></span></a></div>

                            <ul class="site-menu js-clone-nav d-none d-lg-block">
                                <li><a href="<?= $url; ?>">Home</a></li>
                                <li><a href="<?= $url; ?>quem-somos">Quem Somos</a></li>
                                <li class="has-children">
                                    <a href="<?= $url; ?>informacoes">Informações</a>
                                    <ul class="dropdown arrow-top">
                                        <?php include('inc/vetKey.php');
                                        foreach ($vetKey as $key => $value) { ?>
                                        <li><a href="<?=$url.$value["url"];?>"><?=$value['key']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </li>

                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (($title != "Home") && ($pagInterna =="")) { ?>
<section class="mb-5 banner-brad">
    <div class="w-100 h-100" style="background:rgba(0,0,0,0);padding:50px 0px">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1 class="text-white text-center text-uppercase" style="text-shadow: 1px 1px 2px #000000;"><?= $h1; ?></h1>
                    <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                    <?php include 'inc/breadcrumb.php' ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>
