<footer class="site-footer border-top py-4">
    <a href="#topo" class="scrollsuave"><i class="fa fa-chevron-up text-center scrolltop"></i></a>
    <div class="container">
        <div class="row pt-3">
            <div class="col-md-6 col-5 text-center pb-4">
                <img src="<?= $url; ?>images/logo-branca.png" class="logo-footer" alt="Consulta Ideal">
            </div>
            <div class="col-md-6 col-7 text-center pb-4">
               <ul class="d-flex justify-content-center redes mt-2">
                        <li class="d-flex justify-content-center align-items-center">
                            <a rel="nofollow" href="https://www.facebook.com/consultaideal/" target="_blank">
                                <i class="fab fa-facebook"></i>
                            </a>
                        </li>
                        <li class="d-flex justify-content-center align-items-center">
                            <a rel="nofollow" href="https://www.instagram.com/consultaideal/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li class="d-flex justify-content-center align-items-center">
                            <a rel="nofollow" href="https://www.blog.idealconsulta.com.br/" target="_blank">
                                <i class="fab fa-blogger-b"></i>
                            </a>
                        </li>
                        <li class="d-flex justify-content-center align-items-center">
                            <a rel="nofollow" href="mailto:atendimento@idealconsulta.com.br" target="_blank">
                                <i class="fas fa-envelope"></i>
                            </a>
                        </li>
                    </ul>
            </div>
            <div class="col-md-12  footer-nav ">
                <ul class="pages border-footer">
                    <li><a rel="nofollow" title="Home" href="<?= $url; ?>">Home</a></li>
                    <li><a rel="nofollow" title="Empresa" href="<?= $url; ?>quem-somos">Quem Somos</a></li>
                    <li><a rel="nofollow" title="Informações" href="<?= $url; ?>informacoes">Informações</a></li>
                    <li><a title="Mapa do site" href="<?= $url; ?>mapa-site">Mapa do site</a></li>
                </ul>
            </div>

        </div>
        <div class="row">
            <div class="col-md-6 text-center text-md-left">
                <p>

                    Copyright &copy;
                    <script>
                        document.write(new Date().getFullYear());

                    </script> Todos os direitos reservados | Consulta Ideal

                </p>
            </div>
            <div class="col-md-6 selos text-center text-md-right">
                <a class="mr-2" rel="noopener nofollow" href="https://validator.w3.org/check?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>" target="_blank" title="HTML5 W3C">
                
                <span class="fab fa-html5"></span>
                
                <strong>W3C</strong></a>
                <a class="ml-2" rel="noopener nofollow" href="https://jigsaw.w3.org/css-validator/validator?uri=<?=$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>&profile=css3svg&usermedium=all&warning=1&vextwarning=&amp;lang=pt-BR" target="_blank" title="CSS W3C">
               
                    <span class="fab fa-css3"></span>
              
                
                <strong>W3C</strong></a>
            </div>

        </div>
    </div>
</footer>
<?php include "inc/LAB.php"; ?>