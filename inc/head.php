<?php include('inc/geral.php'); ?>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="<?=$url?>images/favicon.png">
<title><?=$title." - ".$nomeSite?></title>

   <style>
	   <?
	   include ("css/bootstrap.min.css");
	   include ("css/style.css");
	   include ("css/owl.carousel.min.css");
	   include ("css/owl.theme.default.min.css");
	   ?>
   </style>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito+Sans:200,300,400,700,900"> 



<?php  if ($h1 == "Informações"){?>
<link rel="stylesheet" href="<?=$url?>css/jquery.paginate.css">
<?php } ?>



<link rel="stylesheet" href="<?=$url?>css/owl.carousel.min.css">
<link rel="stylesheet" href="<?=$url?>css/owl.theme.default.min.css">

<base href="<?=$url;?>">
<meta name="description" content="<?=ucfirst($desc)?>">
<meta name="keywords" content="<?=str_replace($prepos,', ', $h1).', '.$nomeSite?>">
<meta name="geo.position" content="<?=$latitude.";".$longitude?>">
<meta name="geo.placename" content="<?=$cidade."-".$uf?>">
<meta name="geo.region" content="<?=$uf?>-BR">
<meta name="ICBM" content="<?=$latitude.";".$longitude?>">
<meta name="robots" content="index,follow">
<meta name="rating" content="General">
<meta name="revisit-after" content="7 days">
<link rel="canonical" href="<?=$url.$urlPagina?>">
<meta name="author" content="<?=$nomeSite?>">
<link rel="shortcut icon" href="<?=$url?>images/favicon.png">
<meta property="og:region" content="Brasil">
<meta property="og:title" content="<?=$title." - ".$nomeSite?>">
<meta property="og:type" content="article">
<meta property="og:image" content="<?=$url?>images/logo.png">
<meta property="og:url" content="<?=$url.$urlPagina?>">
<meta property="og:description" content="<?=$desc?>">
<meta property="og:site_name" content="<?=$nomeSite?>">  