<?php

$nomeSite = "Consulta Ideal";
$slogan = "A plataforma completa para cuidar da sua saúde bucal";


if ($_SERVER['HTTP_HOST'] == "localhost"){
    $url = "https://localhost/projetos/dentistaandre.com.br/";
} else if ($_SERVER['HTTP_HOST'] == "servertemporario.com.br"){
    $url = "https://servertemporario.com.br/projetos/dentistaandre.com.br/";
} else {
    $url = "https://www.dentistaandre.com.br/";
}


$emailContato = "contato@doutoresdaweb.com.br";

$rua = "Av. Guido Caloi, 1985";
$bairro = "Santo Amaro";
$cidade = "São Paulo";
$UF = "SP";
$cep = "01234-567";
$latitude = "";
$longetude = "";
$mapa = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3654.3799098578083!2d-46.72691788505449!3d-23.662368184632818!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce510a09d3527d%3A0x2fccc309d492596b!2sAv.+Guido+Caloi%2C+1985+-+Jardim+S%C3%A3o+Lu%C3%ADs+(Zona+Sul)%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1547498712635" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>';

if ($_SERVER['HTTP_HOST'] != "localhost"){
    $idAnalytics = "UA-125842810-17";
}else{
    $idAnalytics = "";  
}



$siteKey = '6Lfc7g8UAAAAAHlnefz4zF82BexhvMJxhzifPirv';
$secretKey = '6Lfc7g8UAAAAAKi8al32HjrmsdwoFoG7eujNOwBI';
$creditos	= 'Doutores da Web - Marketing Digital';
$siteCreditos	= 'www.doutoresdaweb.com.br';


$explode	= explode("/", $_SERVER['PHP_SELF']);
$urlPagina = end($explode);
$urlPagina	= str_replace('.php','',$urlPagina);
$urlPagina == "index"? $urlPagina= "" : "";


//Gerar htaccess automático

$urlhtaccess = $url;
$schemaReplace = strpos($urlhtaccess, 'https://www.') === false ? 'https://' : 'https://www.';
$urlhtaccess = str_replace($schemaReplace, '', $urlhtaccess);
$urlhtaccess = rtrim($urlhtaccess,'/');
define('RAIZ', $url);
define('HTACCESS', $urlhtaccess);
include('inc/gerador-htaccess.php');

include ('inc/titletourl.php');
$prepos = array(' a ',' á ',' à ',' ante ',' até ',' após ',' de ',' desde ',' em ',' entre ',' com ',' para ',' por ',' perante ',' sem ',' sob ',' sobre ',' na ',' no ',' e ',' do ',' da ',' ','(',')','\'','"','.','/',':',' | ', ',, ');

$isMobile =  preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);

?>