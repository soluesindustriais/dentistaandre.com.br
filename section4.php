<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h2 class="font-weight-bold text-black mb-5 h3 text-uppercase">Você merece se sentir bem sorrindo!</h2>
            </div>
        </div>
        <div class="row ">
            <?php  
            $palavraEstudo_s8 = array(
                
                'consultório dentista',
                'consultórios odontológicos',
                'dentista emergência',
                'dentista 24 horas zona leste',
                'clínica odontológica 24 horas',
                'consultório dentário',
                'clínica de dentista',
                'dentista 24 horas zona sul'
            );
            
            include 'inc/vetKey.php';            
           
            foreach ($vetKey as $key => $value) {

            if(in_array(strtolower($value['key']), $palavraEstudo_s8)){  ?> 
                
            <div class="col-lg-3 col-md-6 mb-4">
                <div class="col-12 post-entry p-0  bg-white" style="box-shadow: 0 0px 10px rgba(0, 0, 0, 0.25);">
                    <a href="<?=$url.$value["url"];?>" class="d-block">
                        <img src="<?=$url?>images/img-mpi/350x350/<?=$value['url']?>-1.jpg" alt="<?=$value['key']?>" class="img-fluid" title="<?=$value['key']?>">
                    </a>            
                    <h3 class="p-3 m-0 text-uppercase text-center d-flex justify-content-center align-items-center" style="min-height:76px"><a href="<?=$url.$value["url"];?>"><?=$value['key']?></a></h3>
                </div>
            </div>
            
    <?php } } ?>
        </div>

    </div>
</div>
