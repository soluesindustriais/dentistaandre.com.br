<?php
include('inc/vetKey.php');
$h1 = "dentista infantil 24 horas";
$title = $h1;
$desc = "Dentista infantil 24 horas A odontopediatria é, como o próprio nome já indica, uma especialidade odontológica focada no atendimento de crianças de";
$key = "dentista,infantil,24,horas";
$legendaImagem = "Foto ilustrativa de dentista infantil 24 horas";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista infantil 24 horas</h2><p>Quando falamos de dentista infantil 24 horas, falamos sobre uma área diferenciada da odontologia, a área especializada com o atendimento em bebes, crianças e adolescentes. Muitos pais precisam ter a consciência que a boa saúde bucal desses jovens, não são total responsabilidades do dentista, e sim dos pais também, que precisam orientar dentro de casa o que foi passado no consultório do dentista infantil 24 horas.</p><p>Analisando essas informações, fica ainda mais claro do porquê é tão importante cuidar da saúde bucal desde cedo. Quando antes a criança criar o hábito de ir até o dentista infantil 24 horas, antes ela estará livre de doenças e garantirá uma boca mais saudável para o resto da vida.</p><h2>Quando deve ocorrer a primeira consulta ao dentista infantil 24 horas?</h2><p align="center"></p><p>Não há uma data exata para levar as crianças para a primeira consulta com um dentista infantil 24 horas nem somente em casos emergenciais. O recomendado, no entanto, é que isso seja feito o quanto antes. Caso seu filho tenha mais de 1 ano de idade, por exemplo, o acompanhamento é essencial.</p><p>Porém, é interessante tanto para o dentista quando para a criança que seja levada o quanto antes para sua consulta. Não sendo necessária a aparição de qualquer problema ou até mesmo, o nascimento dos primeiros dentes. </p><p>O melhor método para evitar qualquer tipo de doença é sem duvida com a prevenção, pensando assim, quanto antes os pais encaminharem seus filhos as visitas rotineiras aos dentistas, mais chances de descobrir algo errado ou até mesmo evitar que qualquer coisa apareça.</p><p></p><h2>Problemas bucais comuns durante a infância</h2><p align="center"></p><p>Não precisa ser nenhum especialista para saber os problemas que alimentos como doces, balas, e muito excesso de açúcar, combinado com uma má escovação pode causar nos dentes.</p><p>Não precisa ser nenhum especialista para saber os problemas que alimentos como doces, balas, e muito excesso de açúcar, combinado com uma má escovação pode causar nos dentes.</p><ul><li>Grande quantidade de cáries, espalhadas na maioria dos dentes;</li></ul><ul><li>Surgimento das placas bacterianas, principalmente nos dentes do fundo da boca;</li></ul><ul><li>A fluorese, manchando os primeiros dentes de leite da criança;</li></ul><ul><li>E um dos mais gravas, a perda dental, devido a hábitos não saudáveis de escovação.</li></ul><h2>Como os pais podem participar e favorecer a saúde bucal infantil?</h2><p align="center"></p><p>É sempre muito importante que os pais acompanhem seus filhos em toda a fase de crescimento, quando a saúde bucal está envolvida, isso não é diferente. Quando a criança passa para a adolescência, é nessa fase que ela precisa adquirir todos os hábitos saudáveis de higiene bucal, e fazer com que aquilo vire rotina em sua vida.</p><p>Sem contar o cuidado com a alimentação, que deve ser vigiado de perto, sempre. Evitando o excesso de alimentos que podem prejudicar rapidamente a saúde, evitando incômodos futuros maiores. Todos esses cuidados em casa junto ao acompanhamento ideal de um bom dentista infantil 24 horas, podem fazer a total diferença no crescimento saudável de seu filho, resultando em uma boca limpa e sem doença na sua vida adulta.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
