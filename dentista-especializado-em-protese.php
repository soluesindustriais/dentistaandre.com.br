<?php
include('inc/vetKey.php');
$h1 = "dentista especializado em prótese";
$title = $h1;
$desc = "Dentista especializado em prótese A prótese dentária é a especialidade do dentista especializado em prótese responsável pela reabilitação dos";
$key = "dentista,especializado,em,prótese";
$legendaImagem = "Foto ilustrativa de dentista especializado em prótese";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista especializado em prótese</h2><p>O dentista especializado em prótese é o responsável por proporcionar um novo sorriso a pacientes, dando reabilitação para aqueles que já perderam os dentes. A principal função é, devolver a função de mastigar que os dentes tem, só que como próteses, além de devolver o sorriso e toda autoestima com a parte estética.</p><p>As próteses podem ser de tipos diferentes, sendo fixas, removíveis, sobre implantes ou sobre os dentes. O material também tem variação, sendo feitas ou de metal, resina ou até mesmo de porcelana. O correto é o dentista especializado em prótese apontar qual melhor se encaixa ao atual problema do paciente.</p><h2>Prótese Fixa, como funciona?</h2><p align="center"></p><p>Quando falado de prótese dentária, a prótese fixa é uma das mais procuradas pelos pacientes. Tendo a característica de serem múltiplas ou unitárias. As unitárias, tem melhor uso quando a raiz do paciente esta em estado recuperável. Assim, é feita a coroa de porcelana envolvendo toda essa raiz. Caso haja a extração da raiz, é realizado primeiro um implante para depois realizar o implante da coroa.  </p><p>Alguns casos há mais de um dente afetado, nesses casos é comum fazer uma prótese fixa, que fique apoiada nos dentes ao lado, ou até mesmo sobre o implante. Além dessas duas próteses temos também a prótese total fixa sobre implantes, chamada por muitos especialistas de protocolo. Usada em casos extremos, quando o paciente não possui mais dente em sua arcada dentária.</p><h2>Quatro benefícios de um dentista especializado em prótese para sua saúde</h2><p align="center"></p><p>O implante de prótese dentária é importantíssimo para quem sofre ou já sofreu com a perda dos dentes por qualquer que seja o motivo. A prótese devolve ao paciente funções e hábitos que não tinha mais, coisas simples como mastigar. Pensando desta forma, veja os benefícios de procurar um bom dentista especializado em prótese:</p><ul><li>Devolve a mastigação;</li></ul><p>Todos os dentes em nossa boca tem suas determinadas funções, por isso, a perda de um dente que seja pode gerar prejuízos. O organismo pode ser afetado diretamente, já que, a mastigação tem influência direta no nosso sistema digestório. </p><ul><li>Melhora a pronúncia;</li></ul><p>Os dentes em nossa boca, tem relação direta com a fala, já que, os fonemas de nossa linguá dependem diretamente dos dentes. Somente assim a verbalização se torna adequada.</p><ul><li>Alinhamento dos dentes;</li></ul><p>Para a estabilizar os dentes que ainda estão na boca, a reposição dos que foram perdidos devem ser realizadas o mais rápida possível. Toda vez que há um espaço livre, o dente tem a características de se movimentar, preenchendo esse espaço.</p><ul><li>Devolver a autoestima.</li></ul><p>Uma das principais preocupações de quem sofre com a perda dos dentes é a baixa autoestima. A parte estética da pessoa é muito afetada, trazendo total desconforto, tanto na vida pessoal quanto na profissional. Além da parte estética, a prótese pode evitar que os músculos da face fiquem flácidos, dando assim uma aparência  mais envelhecida.</p><h2>Quanto tempo dura uma prótese?</h2><p>É comum ouvir que uma prótese pode durar a vida toda, porém não é bem assim que funciona. Como qualquer outro tipo de prótese, ela tem sua vida útil e requer manutenção, tendo prazo para ser trocado em determinado tempo. Em média uma prótese deve ser trocada a cada 5 anos.</p><p>Algumas próteses duram mais, que é o caso da fixa, que tem como duração um período maior de 8 até 10 anos, dependendo muito do caso em que a prótese foi implantada. Mas somente quem pode dar o tempo certo para troca é o dentista especializado em prótese.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
