<?php
include('inc/vetKey.php');
$h1 = "dentista urgência 24 horas";
$title = $h1;
$desc = "Para que serve um dentista urgência 24 horas? Toda situação que exige tratamento imediato é chamada de emergência. Quando esta situação ocorre";
$key = "dentista,urgência,24,horas";
$legendaImagem = "Foto ilustrativa de dentista urgência 24 horas";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Para que serve um dentista urgência 24 horas?</h2><p align="center"></p><p>Todos sabemos quando acontece alguma situação real de emergência, e quando essa situação acontece diretamente a boca, é chamada de emergência odontológica. Quando um acidente do tipo acontece, é quase certo que o atendimento a uma clínica de dentista urgência 24 horas tende ser na hora, sem poder esperar até o dia seguinte para uma consulta. Esses problemas que fazem parte de um dentista urgência 24 horas, são:</p><p></p><ul><li>Dores extremamente fortes;</li></ul><ul><li>Traumas ou fraturas;</li></ul><ul><li>Dente apresentando estar frouxo;</li></ul><ul><li>A perda de um dente dado a uma pancada violenta;</li></ul><ul><li>Qualquer tipo de fratura da mandíbula.</li></ul><p>E certas complicações que ocorrem após um tratamento odontológico. Esses problemas apresentados não são de fatos letais, porém podem alavancar em outras causas maiores, dai os devidos cuidados.</p><p>Como todo sintoma diferente que não esta correto em nosso corpo, é necessário procurar um médico, com a boca não é diferente. No primeiro sinal forte de dor de dente, é sempre útil procurar um bom dentista urgência 24 horas.</p><p>Em diversos casos, as dores são mais fortes durante a noite, quando deitamos.  Quando a fonte dessa dor vem de algum nervo do dente, o melhor a se fazer é realizar um tratamento de canal, removendo a polpa que está infeccionada e aliviando a dor após um procedimento bem feito.</p><h2>Eu devo usar algum método para aliviar a dor?</h2><p>Não aplique remédios no dente. Não faça bochechos com anestésico, com cachaça, ou qualquer outra bebida alcoólica, vinagre ou qualquer outra substância. Além de não ter resultado, ainda podem provocar lesões na bochecha ou gengiva (úlceras, aftas e até necrose do tecido). Alguns tipos de chá podem causar certo alívio, mas se a dor for aguda só com a intervenção do dentista. Não faça automedicação. Não tome antibióticos por conta própria, normalmente só provocam efeito após 24 horas e podem agravar o problema.</p><p>Depois de qualquer tipo de procedimento de urgência realizado,  é fundamental seguir a risca dois pontos, a medicação indicada pelo dentista urgência 24 horas e claro, o repouso. O repouso por mais que não tenha a devida atenção, é essencial. Evitar mastigar alimentos duros também é importante, ainda mais se o procedimento realizado estiver com pontos internos.</p><h2>Dor de dente nascendo, como reagir?</h2><p>Sempre que há um dente para nascer, os incômodos já começam. O nascimento de um dente, gera o rompimento da pele da gengiva, oque provoca uma dor forte para o paciente. Mas essa situação pode ser bem pior, caso haja a inflamação da gengiva. Inflamação essa que pode ocorrer devido aos péssimos cuidados bucais, como a falta de higiene. </p><p>Para evitar que isso ocorra, o melhor e mais fácil procedimento é sempre manter uma boa higienização da boca, fazendo uso de pastas apropriadas, escovas que não afetam a carne da gengiva, bochecho com enxaguantes bucais e o bom e velho fio dental. Caso você siga esses passos e as dores ainda sejam continuas, o melhor a se fazer é procurar um bom dentista urgência 24 horas. Lembrando que somente o tratamento adequado pode resultar na total remoção do problema.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
