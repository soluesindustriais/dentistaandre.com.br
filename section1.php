<div class="site-section">
    <div class="container">
        <div class="row">

            <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                <div class="media custom-media d-flex align-items-center">
                    <div class="mr-3 icon"><span class="display-4"><img src="<?= $url; ?>images/icone1.png" alt="tratamento odontológico"></span></div>
                    <div class="media-body">                
                        <p class="text-black m-0">Busque mais de 15 especialidades odontológicas em uma plataforma moderna.</p>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                <div class="media custom-media d-flex align-items-center">
                    <div class="mr-3 icon"><span class="display-4"><img src="<?= $url; ?>images/icone2.png" alt="profissionais qualificados"></span></div>
                    <div class="media-body">
                        <p class="text-black m-0">Dentistas qualificados em diversas especialidades para você e toda família.</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 mb-5 mb-lg-0">
                <div class="media custom-media d-flex align-items-center">
                    <div class="mr-3 icon"><span class="display-4"><img src="<?= $url; ?>images/icone3.png" alt="clínicas odontológicas"></span></div>
                    <div class="media-body">
                        <p class="text-black m-0">Encontre as clínicas modernas e marque uma consulta da forma mais fácil.</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>