<?php
include('inc/vetKey.php');
$h1 = "dentista especializado em canal";
$title = $h1;
$desc = "Dentista especializado em canal O tratamento de canal é um dos procedimentos bucais mais temidos nos consultórios odontológicos. Com uma fama de doer";
$key = "dentista,especializado,em,canal";
$legendaImagem = "Foto ilustrativa de dentista especializado em canal";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista especializado em canal</h2><p align="center"></p><p>O tratamento de canal é um dos procedimentos bucais mais temidos nos consultórios odontológicos. Com uma fama de doer e ter um pós-operatório que necessita de muitos cuidados, ela costuma assustar algumas pessoas. Indicado para tratar uma lesão ou cárie significativa que danificou a polpa do dente, não dá para fugir do tratamento quando indicado pelo seu dentista. Entenda mais como funciona a cirurgia e veja dicas para aquelas pessoas que ficam nervosas só de pensar nesse procedimento.</p><p></p><p>O canal é formado por um espaço na parte interna do dente, deixando um vaso linfático. Quando esse nervo esta inflamado por conta de determinadas bactérias, é necessário que o dentista especializado em canal realize a abertura deste dente, removendo todas as infecções que há nele, e depois preencher o espaço com uma resina apropriada, conhecida por muitos como obturação. Somente assim a infecção pode ser controlada e eliminada com total eficácia.</p> <h2>Principais dicas para quem está inseguro com o tratamento de canal</h2><p align="center"></p><p>É muito comum que o paciente tenha medo de realizar o tratamento de canal, por muitos motivos, de já ouvir falar em algum que não deu certo, de ter feito um procedimento mal sucedido no passado, mais atualmente com os avanços da odontologia esse medo não é necessário. O primeiro passo para garantir o melhor sucesso do tratamento é sem dúvida procurar um dentista especializado em canal. Entrar em contato com seu plano odontologia e buscar informações pode ser um bom começo, ou caso queira fazer a opção por um dentista particular, com indicação de alguém que já tenha realizado o tratamento com o mesmo.</p><p>Atualmente, o tratamento de canal é feito após a anestesia local aplicada, como todos os outros procedimentos odontológicos. O dentista especializado em canal realiza a aplicação na hora, evitando qualquer incomodo durante o procedimento. Se informar bem é muito útil também para perder o medo de realizar o tratamento, e ver que apenas benefícios serão tragos para sua vida depois de passar por um bom profissional.</p><p> Realizar o tratamento com um dentista especializado em canal oferece grandes vantagens, tais como:</p><ul><li>Impede que haja cáries;</li></ul><ul><li>Não permite a queda de dentes;</li></ul><ul><li>O dente volta a mastigar de forma correta;</li></ul><ul><li>Após o tratamento, o paciente tende melhorar sua higiene bucal.</li></ul><h2>Por que o procedimento causa medo nas pessoas?</h2><p align="center"></p><p>É normal que o procedimento cause um certo incomodo ao paciente, já que, a areá do canal é envolvida por diversas terminações nervosas, sendo assim, um local mais sensível. A odontologia tem crescido cada vez mais, e um dos procedimentos que mais houve mudanças foi justamente no tratamento de canal. Se colher depoimentos de dentistas formados a anos atrás, eles vão alertar da dificuldade que era realizar um tratamento de canal mais complexo. Devido à falta de aparelhos apropriados para o tal. </p><p>A principal mudança vem da anestesia local, aplicada momentos antes de se iniciar o procedimento, diretamente pelo dentista especializado em canal. A anestesia deixa toda a área em total dormência, impossibilitando que o paciente sinta que o dentista está em contato com os dentes ou gengivas. Graças a isso, o paciente tem um conforto enorme para realizar o procedimento de forma mais tranquila e eficaz.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
