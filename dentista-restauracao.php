<?php
include('inc/vetKey.php');
$h1 = "dentista restauração";
$title = $h1;
$desc = "Dentista restauração: Geralmente o dentista restauração a indica quando a perda de um dente ou mais ou até mesmo se uma cárie estiver em um nível muito alto";
$key = "dentista,restauração";
$legendaImagem = "Foto ilustrativa de dentista restauração";
$pagInterna = "Informações";
$urlPagInterna = "informacoes";
?>
    <!DOCTYPE html>
    <html lang="pt-br">

    <head>
        <?php include("inc/head.php"); ?>
        <link rel="stylesheet" href="<?=$url?>css/style-mpi.css">
    </head>

    <body>
        <div class="site-wrap">

            <?php include("inc/header.php"); ?>
            <?php include("inc/lp-mpi.php"); ?>

            <div class="container">
                <div class="row">
                    <div class="col-12 mt-1">
                        <?php if(isset($pagInterna) && ($pagInterna !="")){ $previousUrl[0] = array("title" => $pagInterna); } ?>
                        <?php include 'inc/breadcrumb.php' ?>
                    </div>
                    <div class="col-12 mt-3">
                        <h1 class="text-uppercase">
                            <?= $h1; ?>
                        </h1>
                    </div>
                    <article class="col-md-9 col-12 text-black">
                        <?php $quantia = 3; $j=1;
                        include('inc/gallery.php'); ?>

                    <!--StartFragment--><h2>Dentista restauração</h2><p align="center"></p><p>Geralmente o dentista restauração a indica quando a perda de um dente ou mais ou até mesmo se uma cárie estiver em um nível muito alto, e o tratamento for praticamente impossível. São comuns os problemas causados pelas estruturas dentais, esses problemas não ficam apenas no lado estético.</p><p>Qualquer trauma causado na região dos dentes pode afetar o restante da boca, resultando em outros problemas. Pensando desta forma, a restauração pode ser um procedimento importantíssimo para solucionar diversos tipos de casos mais sérios.</p><p>Como é apontado por profissionais, a restauração tem como intuito a reconstrução dos dentes junto a arcada dentária. Este processo tem como função restabelecer a função de cada um dos dentes que foi afetado por algum tipo de doença, sem contar na parte estética. Esses tipos de restaurações consistem em dois modos, as diretas e indiretas. </p><p>A restauração direta são aplicadas quando o material pode ser colocado diretamente no dente a ser restaurado. Já no caso das indiretas, são feitas quando há um maior desgaste do dente ou uma cavidade avantajada.</p><p>Existem alguns tipos de materiais para realizar o procedimento pelo dentista restauração, são eles:</p><ul><li>Amálgama;</li></ul><ul><li>Ouro;</li></ul><ul><li>Resina composta;</li></ul><ul><li>Porcelana.</li></ul><h2>Veja os benefícios de trocar a restauração quando necessário?</h2><p align="center"></p><p>O cuidado com a  restauração não acaba quando o procedimento é encerrado. Da mesma forma que implante, por exemplo, ela necessita ser trocada, seja de material de amálgama ou resina.</p><p>Quando a restauração é de amálgama, o profissional por diversas vezes prefere mantê-la por mais tempo. Por ser de uma cor parecida com a do dente fica muito mais complicado localiza-la. Já a resina é um material menos resistente, podendo sofrer um maior desgaste, fazendo assim, que o dente possa ser atingido por cáries novamente. Independente de qual seja o tipo de restauração, manter contato com o dentista restauração pode manter sua boca saudável também.</p> <h2>Por que devemos trocar as restaurações?</h2><p>As restaurações de amálgama estão com os dias contados, as famosas restaurações ainda se encontrarão no sorriso de muita gente, mas em breve isso acabará. Por isso o mais indicado para pacientes que possuem tal restauração, é fazer a transformação para resina, evitando qualquer problema futuro.</p><p>Graças a isso, essas restaurações vêm sendo trocadas, muitas delas vem causando problemas de infecção devido ao material. Para definir qual o melhor plano a se tomar, avalie com seu dentista de confiança qual procedimento vão recorrer.</p><p>Mas a restauração de resina também não é totalmente eficaz, é sempre bom ficar de olho em seu estado atual. A resina apresenta desgaste com o passar do tempo, podendo também ter pequenas rachaduras, que possibilitam a entrada de bactérias nos dentes, podendo haver cáries. Ainda vale se lembrar que, mesmo uma pessoa com a higiene bucal totalmente bem feita, o desgaste da resina ainda ocorre, aparecendo manchas.</p><p>Lembrando que, o procedimento de troca só é realizado se seu dentista restauração achar que há essa necessidade do mesmo, caso contrario a restauração deve ser mentida até a segunda ordem do dentista restauração.</p><!--EndFragment-->

                    </article>
                    <?php include('inc/coluna-lateral.php'); ?>
                    <?php include('inc/paginas-relacionadas.php'); ?>
                    <?php include('inc/regioes.php'); ?>
                    <?php include('inc/copyright.php'); ?>
                </div>
            </div>
            <?php include("inc/footer.php"); ?>

        </div>

    </body>

    </html>
